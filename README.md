# README #

This repository contains Proximi.IO Android SDK.

### Installation ###

* Clone this repository
* Copy proximiioandroidlibrary-release.aar into **libs** folder of your project/module
* Add following statement to top level of project/module **build.gradle** file 

```
#!java

	repositories {
		mavenCentral()
		flatDir {
			dirs "libs"
		}
	}

```

* Add following dependencies in **build.gradle**


```
#!java
	compile "com.android.support:appcompat-v7:22.2.0"
	compile "com.google.android.gms:play-services-base:7.5.0"
	compile "com.google.android.gms:play-services-location:7.5.0"
	compile "com.navtureapps.proximiiolibrary:proximiioandroidlibrary-release@aar"
```